
public class UD4_1 {

	public static void main(String[] args) {
		final int A = 8;
		final int B = 4;
		
		System.out.println("La suma es " + (A+B)); // Sin el parentésis se concatenaría
		System.out.println("La resta es " + (A-B)); 
		System.out.println("La multipliación es " + (A*B)); 
		System.out.println("La división es " + (A/B)); 
		System.out.println("El módulo  es " + (A%B)); 
	}

}
